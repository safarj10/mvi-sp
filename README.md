# Multi-task learning v klasifikaci RTG snímků

## Zadání práce

Cílem práce je porovnat úspěšnost klasifikace abnormalit na RTG snímcích horní končetiny pomocí běžného single-task learningu, a po té s využitím rozšířeného multi-task learningu. Ten bude obsahovat druhý podpůrný úkol v podobě klasifikace snímku do jedné ze sedmi kategorií podle toho, o jakou část horní končetiny se jedná. Pro trénování bude použit [MURA dataset](https://stanfordmlgroup.github.io/competitions/mura/), který obsahuje 40 561 snímků rozřazených do dvou skupin v závislosti na tom, zda se na nich nachází či nenachází muskuloskeletální abnormalita. Architektura modelu bude inspirována baseline modelem popsaném v původní práci věnované datasetu. Pro trénování bude dále použita augmentace obrázků v podobě náhodných rotací a horizontálních převrácení s následným ořezem a vyvážením barev.

## Získání vstupních dat

O trénovací data je nutné zažádat pomocí formuláře na [webových stránkách](https://stanfordmlgroup.github.io/competitions/mura/) soutěže věnováne MURA datasetu. Po schválení žádosti bude na uvedený e-mail odeslán odkaz umožňující stažení archivu s daty.

## Spuštění notebooku

Implementace je přizpůsobena spouštění v prostředí Google Colab, což se odráží například na procesu načtení dat z datového úložiště Google Drive. Cestu k umístění archivu s trénovacími daty je možné změnit v úvodní části notebooku. Proces trénování lze spustit pomocí metody `run_all`, která přijímá následující parametry:

- `epochs` - nastavení počtu epoch
- `multitask` - přepnutí do multi-task learningu
- `save_metrics` - povolení zapisování metrik do souboru
- `save_model` - povolení ukládání průběžného modelu
