\documentclass[czech]{mvi-report}

\usepackage[utf8]{inputenc} 
\usepackage{url}
\usepackage{tabularx}

\title{Multi-task learning v klasifikaci RTG snímků}

\author{Jan Šafařík}
\affiliation{ČVUT - FIT}
\email{safarj10@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Úvod}

Práce se zabývá použitím přístupu zvaného multi-task learning (MTL) za účelem zlepšení detekce abnormalit u~RTG snímků horních končetin.

Pro účely této práce bude využit MURA (Musculoskeletal Radiographs) dataset obsahující 40~000 rentgenových snímků horní končetiny rozdělených
do dvou kategorií podle toho, zda u~nich byla detekována muskuloskeletální abnormalita. Světově bylo v~roce 2017 těmito obtížemi ovlivněno
1,7 miliard lidí, což vytváří z detekce těchto druhů onemocnění důležitý radiologický úkol. \cite{mura}

V~práci bude nejprve popsána struktura vstupních dat a~proces jejich předzpracování. Následně budou ukázány architektury jednotlivých modelů,
a~to jak základní single-task model predikující pouze přítomnost abnormality, tak multi-task model využívající podpůrného úkolu spočívajícího
v~klasifikaci části horní končetiny. Na závěr budou představeny výsledky provedených experimentů a~bude zhodnocen vliv MTL na detekci abnormalit. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multi-task learning}

Jedná se o~přístup, který namísto fragmentace většího problému na malé nezávislé podproblémy a~jejich odděleného řešení, navrhuje využít současně
několik souvisejících úkolů, které se mohou vzájemně obohacovat o~induktivní bias. Induktivním biasem je myšleno cokoliv, co způsobuje, že
učící algoritmus upřednostní jednu z hypotéz před jinou. Tento přístup může zrychlit proces učení, zvýšit výslednou přesnost nebo
dokonce dovolit vyřešení složitých úkolů, čehož by nebylo možné docílit izolovaně. \cite{multitask-basics}

MTL se vyskytuje buď ve variantě tvrdého nebo měkkého sdílení parametrů ve skrytých vrstvách, přičemž v~této práci bude použita právě první
varianta. Mezi výhody MTL patří například efektivní výběr relevantních features, zvýšená odolnost vůči overfittingu a~snadnější přizpůsobení
modelu novým úkolům souvisejícím s~danou doménou. \cite{multitask-overview}

Tento přístup byl úspěšně použit v~celé řadě oblastí strojového učení, kterými jsou mimo jiné zpracování přirozeného jazyka, rozpoznávání
hlasu či oblast počítačového vidění. Obecně se dá říci, že pokud zároveň optimalizujeme více než jednu ztrátovou funkci, již se jedná
o~multi-task learning. \cite{multitask-overview}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vstupní data}

Vstupní data byla získána po schválení žádosti zaslané skrze elektronický formulář na webové stránce soutěže věnované MURA datasetu. \cite{mura-compet}

Dataset obsahuje 14~863 studií, které jsou rozděleny do sedmi kategorií podle toho, o~jakou část horní končetiny se jedná. Jsou jimi postupně
rameno, pažní kost, loket, předloktí, zápěstí, prsty a~celá ruka. Tyto studie jsou rovněž označeny certifikovanými radiology podle toho, zda se
u~pacienta vyskytuje muskuloskeletální abnormalita či nikoliv. Jednotlivé studie se pak skládají z~jednoho nebo více pohledů, kdy byl typicky pacient
vyfocen rentgenem z~několika různých úhlů. Dataset obsahuje jak trénovací množinu s~celkem 8~200 normálními a~5~117 abnormálními studiemi, tak validační
množinu s~661 normálními a~538 abnormálními studiemi. \cite{mura}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/preprocessing.png}
  \caption{Předzpracování snímků}
  \label{fig:preprocessing}
\end{figure}

\noindent
Vstupní obrázky byly předzpracovány oříznutím v~oblasti, kde se pravděpodobně nachází naskenovaný RTG snímek za pomoci detekce přechodu barev z~černé
do bílé, rovněž byly obrázky zmenšeny na velikost 320x320 a~na závěr byl vylepšen kontrast za účelem zvýraznění hran kostí. To bylo provedeno
aplikováním techniky zvané CLAHE (Contrast Limited AHE), což je varianta techniky AHE (Adaptive Histogram Equalization). V~procesu trénování
byla použita augmentace dat složená z~náhodných horizontálních převrácení či rotací do 30 stupňů. Ukázka předzpracování je vidět na obrázku \ref{fig:preprocessing}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Architektura}

Klasifikace byla prováděna pomocí předtrénované konvoluční neurální sítě (CNN) se 169 vrstvami využívající architektury zvané DenseNet (Densely
Connected Convolutional Network).

Tato architektura se vyznačuje tím, že každá vrstva v~síti je propojena s~každou z~následujících. Vstupem do n-té vrstvy jsou tak detekované features
všech předchozích vrstev, což umožňuje silnější propagaci informací, podporuje znovupoužitelnost již nalezených features a~podstatně redukuje počet
trénovatelných parametrů. \cite{densenet}

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=0.89\linewidth]{img/architecture.pdf}
  \caption{Architektura multi-task modelu}
  \label{fig:architecture}
\end{figure}

\noindent
U~základního single-task modelu byla poslední vrstva nahrazena dvěma vlastními dense vrstvami se 128 a~256 neurony, po kterých následovala
výstupní dense vrstva s~1~neuronem a~sigmoid aktivační funkcí. Pro vyšší odolnost vůči overfittingu byla aplikována dropout vrstva s~parametrem
0,2 a~jako ztrátová funkce byla použita binární cross-entropie porovnávající pravděpodobnost abnormality z~výstupní vrstvy s~ground-truth hodnotou.
Síť byla trénována pomocí algoritmu Adam s~výchozím learning-rate na hodnotě 0,0001 a~postupnou redukcí o~faktor 0,1 po 3~epochách v~ případě
dosažení plateau. Z~důvodu nevyváženého datasetu byla rovněž upravena váha jednotlivých tříd.

Rozšířený multi-task model kromě detekce abnormalit prováděl současně druhý úkol spočívající v~klasifikaci obrázku do jedné ze sedmi kategorií
podle umístění v~oblasti horní končetiny. Oproti základnímu modelu obsahoval další výstupní vrstvu se 7~neurony a~softmax aktivační funkcí, před
níž byla opět aplikována dropout vrstva s~parametrem 0,5. Jako druhá ztrátová funkce byla použita kategorická cross-entropie, přičemž obě funkce
byly vyváženy v~poměru 2:1 tak, aby měla na proces učení primární vliv klasifikace abnormalit. Architektura multi-task modelu je znázorněna na
obrázku \ref{fig:architecture}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vyhodnocení}

Vyhodnocení úspěšnosti obou modelů probíhalo s~využitím Kappa Score, který měří úroveň shody mezi dvěma pozorovateli hodnotícími určité
subjekty. Hodnota 1~znamená perfektní shodu, zatímco hodnota 0~znamená nahodilou shodu. Typicky pak získáváme hodnotu v~intervalu 0,41--0,60
reprezentující mírnou shodu nebo v intervalu 0,61--0,80 reprezentující podstatnou shodu. \cite{kappa-score}

Za účelem výpočtu této metriky byl v~rámci MURA datasetu vytvořen zlatý standard definovaný jako většinový hlas tří náhodně vybraných radiologů.
Hodnota Kappa Score pak určuje míru shody mezi modelem a zlatým standardem. \cite{mura} Pro každou studii z~validační množiny byla postupně
predikována pravděpodobnost abnormality u~všech dostupných pohledů. Tyto hodnoty byly následně zprůměrovány, rozděleny na hranici 0,5 a~porovnány
se zlatým standardem.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Výsledky}

Výsledky experimentů jsou srovnány v~tabulce \ref{fig:results}, kde se nacházejí výsledné hodnoty Kappa Score pro každou ze sedmi kategorií.
V~prvním sloupci jsou zaznamenány výsledky baseline modelu popsaného v~originální práci věnované MURA datasetu, ve druhém sloupci pak výsledky
single-task model a~v~posledním sloupci výsledky multi-task modelu.

\begin{table}[h]
  \begin{tabularx}{\linewidth}{XXXX}
                  & \textbf{Stanford}   & \textbf{STL}    & \textbf{MTL}      \\ \hline
  Rameno          & 0,729               & 0,598           & \textbf{0,618}    \\
  Pažní kost      & 0,600               & 0,763           & \textbf{0,778}    \\
  Loket           & 0,710               & 0,692           & 0,695             \\
  Předloktí       & 0,737               & 0,651           & \textbf{0,696}    \\
  Zápěstí         & 0,931               & 0,731           & 0,730             \\
  Ruka            & 0,851               & 0,507           & \textbf{0,618}    \\ 
  Prst            & 0,389               & 0,596           & \textbf{0,676}    \\ \hline
  Průměr          & 0,705               & 0,648           & \textbf{0,687}                  
  \end{tabularx}
  \caption{Výsledky experimentů \label{fig:results}}
\end{table}

\noindent
Je patrné, že si multi-task model vedl oproti single-task modelu stejně dobře či lépe ve všech sedmi kategoriích, což splnilo původní očekávání.
Oba modely byly učeny po dobu 20 epoch na celé množině trénovacích dat s~batch-size o~velikosti 8~a~vybrán byl pouze ten, který dosahoval
nejvyšší přesnosti na validační množině dat.

Co se týče pažní kosti a prstů ruky, překonal výrazně MTL model původní baseline model a~v~oblasti ramene, lokte a~předloktí se hodnotám značně
přiblížil. Zhoršené výsledky oproti baseline modelu v~ostatních kategoriích mohou být způsobeny odlišnou architekturou dense vrstvy či rozdílným
předzpracováním vstupních dat. Získané hodnoty Kappa Score z~obou experimentů jsou porovnány v~grafu na obrázku \ref{fig:chart}.

\begin{figure}[h]
  \centering\leavevmode
  \includegraphics[width=1\linewidth]{img/chart.pdf}
  \caption{Srovnání výsledků experimentů}
  \label{fig:chart}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Závěr}

Multi-task learning je na základě úspěšných aplikací na poli strojového učení metodou s velkým potenciálem. Proces učení několika souvisejících 
úkolů současně, vedoucích k vzájemnému obohacení, je silně inspirován způsobem, jakým se lidé už od dětství učí novým věcem. 

Úspěšnost MTL modelu navrženého v~této práci by se dala vylepšit použitím dalších úkolů, jako jsou zpětná rekonstrukce původního obrázku pomocí
dekódéru nebo podrobnější klasifikace dle druhu nalezeného defektu, kam mohou patřit například zlomeniny, degenerativní onemocnění kloubů, léze
či subluxace. K~tomu by však bylo potřeba rozšířit MURA dataset o~další hodnoty, které se v něm v~současné době bohužel nevyskytují.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{reference}

\end{document}
